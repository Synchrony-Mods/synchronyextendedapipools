local registration = {}

local tokens = require "SynchronyExtendedAPIPools.Tokens"

local itemPool = require "SynchronyExtendedAPI.pools.ItemPool"
local apiUtils = require "SynchronyExtendedAPI.utils.APIUtils"
local debugConsole = require "SynchronyExtendedAPI.utils.DebugConsole"

local ecs = require "system.game.Entities"
local utils = require "system.utils.Utilities"

local object = require "necro.game.object.Object"
local inventory = require "necro.game.item.Inventory"
local event = require "necro.event.Event"

-- chests
apiUtils.safeAddEvent(
    event.gameStateLevel,
    "changeItemsToTokens",
    {
        order = "level",
        sequence = 1,
    },
    function()
        local markedForDelete = {}
        for _, token in utils.sortedPairs({ tokens.items.TokenChestRed, tokens.items.TokenChestPurple, tokens.items.TokenChestBlack }) do
            local holder = ecs.getEntitiesByType(token.holder)
            while holder:next() do
                -- chests
                for _, itemID  in utils.sortedPairs(holder.inventory.items) do
                    local item = ecs.getEntityByID(itemID)
                    if item:hasComponent("SynchronyExtendedAPI_itemSubstitution") and not item.SynchronyExtendedAPI_itemSubstitution.isFinal then
                        local newItem = object.spawn("SynchronyExtendedAPIPools_" .. token.name, holder.position.x, holder.position.y)
                        inventory.add(newItem, holder, true)
                        table.insert(markedForDelete, itemID)
                    end
                end
            end

            holder = ecs.getEntitiesByType(token.holder .. "Invisible")
            while holder:next() do
                -- chests
                for _, itemID  in utils.sortedPairs(holder.inventory.items) do
                    local newItem = object.spawn("SynchronyExtendedAPIPools_" .. token.name, holder.position.x, holder.position.y)
                    inventory.add(newItem, holder, true)
                    table.insert(markedForDelete, itemID)
                end
            end
        end

        for _, entityID in utils.sortedPairs(markedForDelete) do
            object.delete(ecs.getEntityByID(entityID))
        end
    end)

for _, token in utils.sortedPairs({ tokens.items.TokenChestRed, tokens.items.TokenChestPurple, tokens.items.TokenChestBlack }) do
    for _, replacement in utils.sortedPairs(token.replacements) do
        itemPool.addHolderReplacement(token.holder, "SynchronyExtendedAPIPools_" .. token.name, replacement.template.name, replacement.weight, true)
        itemPool.addHolderReplacement(token.holder .. "Invisible", "SynchronyExtendedAPIPools_" .. token.name, replacement.template.name, replacement.weight, true)
    end
end

function registration.addPoolReplacement(token, newItemName, weight)
    if type(token) ~= "table" then
        debugConsole.printError("Token must be a field from 'SynchronyExtendedAPIPools.Tokens#items")
        return
    end
    
    if not token.holder then
        debugConsole.printError("Token '" .. token.name .. " is not yet supported")
        return
    end

    itemPool.addHolderReplacement(token.holder, "SynchronyExtendedAPIPools_" .. token.name, newItemName, weight, true)
end

return registration