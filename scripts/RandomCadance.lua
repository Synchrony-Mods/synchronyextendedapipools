local tokens = require "SynchronyExtendedAPIPools.Tokens"

local entities = require "SynchronyExtendedAPI.extended.Entities"
local namedPlayers = require "SynchronyExtendedAPI.templates.NamedPlayers"

local entities = require "SynchronyExtendedAPI.extended.Entities"
local itemPool = require "SynchronyExtendedAPI.pools.ItemPool"

local utils = require "system.utils.Utilities"

-- random start character (bard)
entities.addPlayer(namedPlayers.Cadence, {
    name = "RandomCadence",
    data = {},
    components = {
        {
            playableCharacter = {
                lobbyOrder = 0,
            },
            sprite = {
                texture = "ext/entities/player2_armor_body.png"
            },
            bestiary = {
                image = "mods/SynchronyExtendedAPIPools/gfx/bestiary_random_cadence.png"
            },
            initialEquipment = {
                items = {
                    [2] = "SynchronyExtendedAPIPools_TokenHolderShovelAny",
                    [3] = "SynchronyExtendedAPIPools_TokenHolderWeaponAny",
                },
            },
        },
        {
            sprite = {
                texture = "ext/entities/player2_heads.png"
            },
        }
    }
})

for _, replacement in utils.sortedPairs(tokens.items.TokenHolderWeaponAny.replacements) do
    itemPool.addHolderReplacement("RandomCadence", "SynchronyExtendedAPIPools_TokenHolderWeaponAny", replacement.template.name, replacement.weight, true)
end
for _, replacement in utils.sortedPairs(tokens.items.TokenHolderShovelAny.replacements) do
    itemPool.addHolderReplacement("RandomCadence", "SynchronyExtendedAPIPools_TokenHolderShovelAny", replacement.template.name, replacement.weight, true)
end