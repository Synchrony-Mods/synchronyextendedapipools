local tokens = {}

local entities = require "SynchronyExtendedAPI.extended.Entities"
local namedItems = require "SynchronyExtendedAPI.templates.NamedItems"

local utils = require "system.utils.Utilities"

tokens.items = {
    -- Weapon tokens
    TokenHolderWeaponAny = {
        name = "TokenHolderWeaponAny",
        slot = "weapon",
        template = namedItems.WeaponDagger,
        spriteName = "token_weapon.png",
        replacements = {
            -- standard
            { template = namedItems.WeaponDagger, weight = 50, },
            { template = namedItems.WeaponBroadsword, weight = 50, },
            { template = namedItems.WeaponRapier, weight = 50, },
            { template = namedItems.WeaponSpear, weight = 50, },
            { template = namedItems.WeaponLongsword, weight = 50, },
            { template = namedItems.WeaponWhip, weight = 50, },
            { template = namedItems.WeaponBow, weight = 50, },
            { template = namedItems.WeaponCrossbow, weight = 50, },
            { template = namedItems.WeaponCat, weight = 50, },
            { template = namedItems.WeaponFlail, weight = 50, },
            { template = namedItems.WeaponBlunderbuss, weight = 50, },
            { template = namedItems.WeaponRifle, weight = 50, },
            { template = namedItems.WeaponAxe, weight = 50, },
            { template = namedItems.WeaponWarhammer, weight = 50, },
            { template = namedItems.WeaponCutlass, weight = 50, },
            { template = namedItems.WeaponStaff, weight = 50, },
            { template = namedItems.WeaponHarp, weight = 50, },
            
            -- golden
            { template = namedItems.WeaponGoldenDagger, weight = 50, },
            { template = namedItems.WeaponGoldenBroadsword, weight = 50, },
            { template = namedItems.WeaponGoldenRapier, weight = 50, },
            { template = namedItems.WeaponGoldenSpear, weight = 50, },
            { template = namedItems.WeaponGoldenLongsword, weight = 50, },
            { template = namedItems.WeaponGoldenWhip, weight = 50, },
            { template = namedItems.WeaponGoldenBow, weight = 50, },
            { template = namedItems.WeaponGoldenCrossbow, weight = 50, },
            { template = namedItems.WeaponGoldenCat, weight = 50, },
            { template = namedItems.WeaponGoldenFlail, weight = 50, },
            { template = namedItems.WeaponGoldenAxe, weight = 50, },
            { template = namedItems.WeaponGoldenWarhammer, weight = 50, },
            { template = namedItems.WeaponGoldenCutlass, weight = 50, },
            { template = namedItems.WeaponGoldenStaff, weight = 50, },
            { template = namedItems.WeaponGoldenHarp, weight = 50, },

            -- blood
            { template = namedItems.WeaponBloodDagger, weight = 50, },
            { template = namedItems.WeaponBloodBroadsword, weight = 50, },
            { template = namedItems.WeaponBloodRapier, weight = 50, },
            { template = namedItems.WeaponBloodSpear, weight = 50, },
            { template = namedItems.WeaponBloodLongsword, weight = 50, },
            { template = namedItems.WeaponBloodWhip, weight = 50, },
            { template = namedItems.WeaponBloodBow, weight = 50, },
            { template = namedItems.WeaponBloodCrossbow, weight = 50, },
            { template = namedItems.WeaponBloodCat, weight = 50, },
            { template = namedItems.WeaponBloodFlail, weight = 50, },
            { template = namedItems.WeaponBloodAxe, weight = 50, },
            { template = namedItems.WeaponBloodWarhammer, weight = 50, },
            { template = namedItems.WeaponBloodCutlass, weight = 50, },
            { template = namedItems.WeaponBloodStaff, weight = 50, },
            { template = namedItems.WeaponBloodHarp, weight = 50, },

            -- glass
            { template = namedItems.WeaponGlassDagger, weight = 50, },
            { template = namedItems.WeaponGlassBroadsword, weight = 50, },
            { template = namedItems.WeaponGlassRapier, weight = 50, },
            { template = namedItems.WeaponGlassSpear, weight = 50, },
            { template = namedItems.WeaponGlassLongsword, weight = 50, },
            { template = namedItems.WeaponGlassWhip, weight = 50, },
            { template = namedItems.WeaponGlassBow, weight = 50, },
            { template = namedItems.WeaponGlassCrossbow, weight = 50, },
            { template = namedItems.WeaponGlassCat, weight = 50, },
            { template = namedItems.WeaponGlassFlail, weight = 50, },
            { template = namedItems.WeaponGlassAxe, weight = 50, },
            { template = namedItems.WeaponGlassWarhammer, weight = 50, },
            { template = namedItems.WeaponGlassCutlass, weight = 50, },
            { template = namedItems.WeaponGlassStaff, weight = 50, },
            { template = namedItems.WeaponGlassHarp, weight = 50, },

            -- obsidian
            { template = namedItems.WeaponObsidianDagger, weight = 50, },
            { template = namedItems.WeaponObsidianBroadsword, weight = 50, },
            { template = namedItems.WeaponObsidianRapier, weight = 50, },
            { template = namedItems.WeaponObsidianSpear, weight = 50, },
            { template = namedItems.WeaponObsidianLongsword, weight = 50, },
            { template = namedItems.WeaponObsidianWhip, weight = 50, },
            { template = namedItems.WeaponObsidianBow, weight = 50, },
            { template = namedItems.WeaponObsidianCrossbow, weight = 50, },
            { template = namedItems.WeaponObsidianCat, weight = 50, },
            { template = namedItems.WeaponObsidianFlail, weight = 50, },
            { template = namedItems.WeaponObsidianAxe, weight = 50, },
            { template = namedItems.WeaponObsidianWarhammer, weight = 50, },
            { template = namedItems.WeaponObsidianCutlass, weight = 50, },
            { template = namedItems.WeaponObsidianStaff, weight = 50, },
            { template = namedItems.WeaponObsidianHarp, weight = 50, },

            -- titanium
            { template = namedItems.WeaponTitaniumDagger, weight = 50, },
            { template = namedItems.WeaponTitaniumBroadsword, weight = 50, },
            { template = namedItems.WeaponTitaniumRapier, weight = 50, },
            { template = namedItems.WeaponTitaniumSpear, weight = 50, },
            { template = namedItems.WeaponTitaniumLongsword, weight = 50, },
            { template = namedItems.WeaponTitaniumWhip, weight = 50, },
            { template = namedItems.WeaponTitaniumBow, weight = 50, },
            { template = namedItems.WeaponTitaniumCrossbow, weight = 50, },
            { template = namedItems.WeaponTitaniumCat, weight = 50, },
            { template = namedItems.WeaponTitaniumFlail, weight = 50, },
            { template = namedItems.WeaponTitaniumAxe, weight = 50, },
            { template = namedItems.WeaponTitaniumWarhammer, weight = 50, },
            { template = namedItems.WeaponTitaniumCutlass, weight = 50, },
            { template = namedItems.WeaponTitaniumStaff, weight = 50, },
            { template = namedItems.WeaponTitaniumHarp, weight = 50, },
            
            -- special
            { template = namedItems.WeaponEli, weight = 50, },
            { template = namedItems.WeaponFlower, weight = 50, },
            { template = namedItems.WeaponGoldenLute, weight = 50, },
            { template = namedItems.WeaponDaggerJeweled, weight = 50, },
            { template = namedItems.WeaponDaggerFrost, weight = 50, },
            { template = namedItems.WeaponDaggerPhasing, weight = 50, },
            { template = namedItems.WeaponDaggerElectric, weight = 50, },
        },
    },

    TokenHolderWeaponStandard = {
        name = "TokenHolderWeaponStandard",
        slot = "weapon",
        template = namedItems.WeaponDagger,
        spriteName = "token_weapon.png",
        replacements = {
            -- standard
            { template = namedItems.WeaponDagger, weight = 50, },
            { template = namedItems.WeaponBroadsword, weight = 50, },
            { template = namedItems.WeaponRapier, weight = 50, },
            { template = namedItems.WeaponSpear, weight = 50, },
            { template = namedItems.WeaponLongsword, weight = 50, },
            { template = namedItems.WeaponWhip, weight = 50, },
            { template = namedItems.WeaponBow, weight = 50, },
            { template = namedItems.WeaponCrossbow, weight = 50, },
            { template = namedItems.WeaponCat, weight = 50, },
            { template = namedItems.WeaponFlail, weight = 50, },
            { template = namedItems.WeaponBlunderbuss, weight = 50, },
            { template = namedItems.WeaponRifle, weight = 50, },
            { template = namedItems.WeaponAxe, weight = 50, },
            { template = namedItems.WeaponWarhammer, weight = 50, },
            { template = namedItems.WeaponCutlass, weight = 50, },
            { template = namedItems.WeaponStaff, weight = 50, },
            { template = namedItems.WeaponHarp, weight = 50, },
        },
    },

    TokenHolderWeaponGlass = {
        name = "TokenHolderWeaponGlass",
        slot = "weapon",
        template = namedItems.WeaponDagger,
        spriteName = "token_weapon.png",
        replacements = {
            -- glass
            { template = namedItems.WeaponGlassDagger, weight = 50, },
            { template = namedItems.WeaponGlassBroadsword, weight = 50, },
            { template = namedItems.WeaponGlassRapier, weight = 50, },
            { template = namedItems.WeaponGlassSpear, weight = 50, },
            { template = namedItems.WeaponGlassLongsword, weight = 50, },
            { template = namedItems.WeaponGlassWhip, weight = 50, },
            { template = namedItems.WeaponGlassBow, weight = 50, },
            { template = namedItems.WeaponGlassCrossbow, weight = 50, },
            { template = namedItems.WeaponGlassCat, weight = 50, },
            { template = namedItems.WeaponGlassFlail, weight = 50, },
            { template = namedItems.WeaponGlassAxe, weight = 50, },
            { template = namedItems.WeaponGlassWarhammer, weight = 50, },
            { template = namedItems.WeaponGlassCutlass, weight = 50, },
            { template = namedItems.WeaponGlassStaff, weight = 50, },
            { template = namedItems.WeaponGlassHarp, weight = 50, },
        },
    },

    TokenHolderWeaponBlood = {
        name = "TokenHolderWeaponBlood",
        slot = "weapon",
        template = namedItems.WeaponDagger,
        spriteName = "token_weapon.png",
        replacements = {
            -- blood
            { template = namedItems.WeaponBloodDagger, weight = 50, },
            { template = namedItems.WeaponBloodBroadsword, weight = 50, },
            { template = namedItems.WeaponBloodRapier, weight = 50, },
            { template = namedItems.WeaponBloodSpear, weight = 50, },
            { template = namedItems.WeaponBloodLongsword, weight = 50, },
            { template = namedItems.WeaponBloodWhip, weight = 50, },
            { template = namedItems.WeaponBloodBow, weight = 50, },
            { template = namedItems.WeaponBloodCrossbow, weight = 50, },
            { template = namedItems.WeaponBloodCat, weight = 50, },
            { template = namedItems.WeaponBloodFlail, weight = 50, },
            { template = namedItems.WeaponBloodAxe, weight = 50, },
            { template = namedItems.WeaponBloodWarhammer, weight = 50, },
            { template = namedItems.WeaponBloodCutlass, weight = 50, },
            { template = namedItems.WeaponBloodStaff, weight = 50, },
            { template = namedItems.WeaponBloodHarp, weight = 50, },
        },
    },
    TokenHolderWeaponObsidian = {
        name = "TokenHolderWeaponObsidian",
        slot = "weapon",
        template = namedItems.WeaponDagger,
        spriteName = "token_weapon.png",
        replacements = {
            -- obsidian
            { template = namedItems.WeaponObsidianDagger, weight = 50, },
            { template = namedItems.WeaponObsidianBroadsword, weight = 50, },
            { template = namedItems.WeaponObsidianRapier, weight = 50, },
            { template = namedItems.WeaponObsidianSpear, weight = 50, },
            { template = namedItems.WeaponObsidianLongsword, weight = 50, },
            { template = namedItems.WeaponObsidianWhip, weight = 50, },
            { template = namedItems.WeaponObsidianBow, weight = 50, },
            { template = namedItems.WeaponObsidianCrossbow, weight = 50, },
            { template = namedItems.WeaponObsidianCat, weight = 50, },
            { template = namedItems.WeaponObsidianFlail, weight = 50, },
            { template = namedItems.WeaponObsidianAxe, weight = 50, },
            { template = namedItems.WeaponObsidianWarhammer, weight = 50, },
            { template = namedItems.WeaponObsidianCutlass, weight = 50, },
            { template = namedItems.WeaponObsidianStaff, weight = 50, },
            { template = namedItems.WeaponObsidianHarp, weight = 50, },
        },
    },

    TokenHolderWeaponTitanium = {
        name = "TokenHolderWeaponTitanium",
        slot = "weapon",
        template = namedItems.WeaponDagger,
        spriteName = "token_weapon.png",
        replacements = {
            -- titanium
            { template = namedItems.WeaponTitaniumDagger, weight = 50, },
            { template = namedItems.WeaponTitaniumBroadsword, weight = 50, },
            { template = namedItems.WeaponTitaniumRapier, weight = 50, },
            { template = namedItems.WeaponTitaniumSpear, weight = 50, },
            { template = namedItems.WeaponTitaniumLongsword, weight = 50, },
            { template = namedItems.WeaponTitaniumWhip, weight = 50, },
            { template = namedItems.WeaponTitaniumBow, weight = 50, },
            { template = namedItems.WeaponTitaniumCrossbow, weight = 50, },
            { template = namedItems.WeaponTitaniumCat, weight = 50, },
            { template = namedItems.WeaponTitaniumFlail, weight = 50, },
            { template = namedItems.WeaponTitaniumAxe, weight = 50, },
            { template = namedItems.WeaponTitaniumWarhammer, weight = 50, },
            { template = namedItems.WeaponTitaniumCutlass, weight = 50, },
            { template = namedItems.WeaponTitaniumStaff, weight = 50, },
            { template = namedItems.WeaponTitaniumHarp, weight = 50, },
        },
    },

    -- Armor tokens
    TokenHolderArmorAny = {
        name = "TokenHolderArmorAny",
        slot = "armor",
        template = namedItems.ArmorLeather,
        spriteName = "token_armor.png",
        replacements = {
            -- standard
            { template = namedItems.ArmorLeather, weight = 50, },
            { template = namedItems.ArmorChainmail, weight = 50, },
            { template = namedItems.ArmorPlatemail, weight = 50, },
            { template = namedItems.ArmorHeavyplate, weight = 50, },

            -- obsidian
            { template = namedItems.ArmorObsidian, weight = 50, },

            -- glass
            { template = namedItems.ArmorGlass, weight = 50, },
            { template = namedItems.ArmorHeavyglass, weight = 50, },

            -- special
            { template = namedItems.ArmorGi, weight = 50, },
            { template = namedItems.ArmorQuartz, weight = 50, },
        },
    },

    -- Ring tokens
    TokenHolderRingAny = {
        name = "TokenHolderRingAny",
        slot = "ring",
        template = namedItems.RingCourage,
        spriteName = "token_ring.png",
        replacements = {
            { template = namedItems.RingBecoming, weight = 50, },
            { template = namedItems.RingCharisma, weight = 50, },
            { template = namedItems.RingCourage, weight = 50, },
            { template = namedItems.RingFrost, weight = 50, },
            { template = namedItems.RingGold, weight = 50, },
            { template = namedItems.RingLuck, weight = 50, },
            { template = namedItems.RingMana, weight = 50, },
            { template = namedItems.RingMight, weight = 50, },
            { template = namedItems.RingPeace, weight = 50, },
            { template = namedItems.RingPhasing, weight = 50, },
            { template = namedItems.RingPiercing, weight = 50, },
            { template = namedItems.RingProtection, weight = 50, },
            { template = namedItems.RingRegeneration, weight = 50, },
            { template = namedItems.RingShadows, weight = 50, },
            { template = namedItems.RingShielding, weight = 50, },
            { template = namedItems.RingWar, weight = 50, },
            { template = namedItems.RingWonder, weight = 50, },
        },
    },

    -- Shovel tokens
    TokenHolderShovelAny = {
        name = "TokenHolderShovelAny",
        slot = "shovel",
        template = namedItems.ShovelBasic,
        spriteName = "token_shovel.png",
        replacements = {
            { template = namedItems.ShovelBasic, weight = 50, },
            { template = namedItems.ShovelBattle, weight = 50, },
            { template = namedItems.ShovelBlood, weight = 50, },
            { template = namedItems.ShovelCourage, weight = 50, },
            { template = namedItems.ShovelCrystal, weight = 50, },
            { template = namedItems.ShovelGlass, weight = 50, },
            { template = namedItems.ShovelObsidian, weight = 50, },
            { template = namedItems.ShovelStrength, weight = 50, },
            { template = namedItems.ShovelTitanium, weight = 50, },
            { template = namedItems.Pickaxe, weight = 50, },
        },
    },

    -- Torch tokens
    TokenHolderTorchAny = {
        name = "TokenHolderTorchAny",
        slot = "torch",
        template = namedItems.Torch1,
        spriteName = "token_torch.png",
        replacements = {
            { template = namedItems.Torch1, weight = 50, },
            { template = namedItems.Torch2, weight = 50, },
            { template = namedItems.Torch3, weight = 50, },
            { template = namedItems.TorchForesight, weight = 50, },
            { template = namedItems.TorchGlass, weight = 50, },
            { template = namedItems.TorchInfernal, weight = 50, },
            { template = namedItems.TorchObsidian, weight = 50, },
            { template = namedItems.TorchStrength, weight = 50, },
            { template = namedItems.TorchWalls, weight = 50, },
        },
    },

    -- Feet tokens
    TokenHolderFeetAny = {
        name = "TokenHolderFeetAny",
        slot = "feet",
        template = namedItems.FeetBalletShoes,
        spriteName = "token_feet.png",
        replacements = {
            { template = namedItems.FeetBalletShoes, weight = 50, },
            { template = namedItems.FeetBootsExplorers, weight = 50, },
            { template = namedItems.FeetBootsLead, weight = 50, },
            { template = namedItems.FeetBootsLeaping, weight = 50, },
            { template = namedItems.FeetBootsLunging, weight = 50, },
            { template = namedItems.FeetBootsPain, weight = 50, },
            { template = namedItems.FeetBootsSpeed, weight = 50, },
            { template = namedItems.FeetBootsStrength, weight = 50, },
            { template = namedItems.FeetBootsWinged, weight = 50, },
            { template = namedItems.FeetGlassSlippers, weight = 50, },
            { template = namedItems.FeetGreaves, weight = 50, },
        },
    },

    -- Head tokens
    TokenHolderHeadAny = {
        name = "TokenHolderHeadAny",
        slot = "head",
        template = namedItems.HeadHelm,
        spriteName = "token_head.png",
        replacements = {
            { template = namedItems.HeadBlastHelm, weight = 50, },
            { template = namedItems.HeadCircletTelepathy, weight = 50, },
            { template = namedItems.HeadCrownOfGreed, weight = 50, },
            { template = namedItems.HeadCrownOfTeleportation, weight = 50, },
            { template = namedItems.HeadCrownOfThorns, weight = 50, },
            { template = namedItems.HeadGlassJaw, weight = 50, },
            { template = namedItems.HeadHelm, weight = 50, },
            { template = namedItems.HeadMinersCap, weight = 50, },
            { template = namedItems.HeadMonocle, weight = 50, },
            { template = namedItems.HeadNinjaMask, weight = 50, },
            { template = namedItems.HeadSonar, weight = 50, },
            { template = namedItems.HeadSpikedEars, weight = 50, },
            { template = namedItems.HeadSunglasses, weight = 50, },
        },
    },
    
    -- Floor tokens
    TokenFloorAny = {
        name = "TokenFloorAny",
        spriteName = "token_floor.png",
        replacements = {
        },
    },
    
    -- Chest tokens
    TokenChestBlack = {
        name = "TokenChestBlack",
        holder = "ChestBlack",
        spriteName = "token_chest.png",
        replacements = {
            -- armor
            { template = namedItems.ArmorChainmail, weight = 125, },
            { template = namedItems.ArmorGi, weight = 16, },
            { template = namedItems.ArmorGlass, weight = 4, },
            { template = namedItems.ArmorHeavyglass, weight = 4, },
            { template = namedItems.ArmorHeavyplate, weight = 1, },
            { template = namedItems.ArmorLeather, weight = 150, },
            { template = namedItems.ArmorPlatemail, weight = 31, },
            { template = namedItems.ArmorQuartz, weight = 135, },

            -- standard weapons
            { template = namedItems.WeaponAxe, weight = 65, },
            { template = namedItems.WeaponBow, weight = 5, },
            { template = namedItems.WeaponBroadsword, weight = 125, },
            { template = namedItems.WeaponCat, weight = 60, },
            { template = namedItems.WeaponCrossbow, weight = 2, },
            { template = namedItems.WeaponCutlass, weight = 5, },
            { template = namedItems.WeaponFlail, weight = 40, },
            { template = namedItems.WeaponHarp, weight = 40, },
            { template = namedItems.WeaponLongsword, weight = 115, },
            { template = namedItems.WeaponRapier, weight = 65, },
            { template = namedItems.WeaponSpear, weight = 110, },
            { template = namedItems.WeaponStaff, weight = 40, },
            { template = namedItems.WeaponWarhammer, weight = 3, },
            { template = namedItems.WeaponWhip, weight = 55, },

            -- blood weapons
            { template = namedItems.WeaponBloodAxe, weight = 21, },
            { template = namedItems.WeaponBloodBow, weight = 2, },
            { template = namedItems.WeaponBloodBroadsword, weight = 19, },
            { template = namedItems.WeaponBloodCat, weight = 17, },
            { template = namedItems.WeaponBloodCrossbow, weight = 2, },
            { template = namedItems.WeaponBloodCutlass, weight = 2, },
            { template = namedItems.WeaponBloodDagger, weight = 26, },
            { template = namedItems.WeaponBloodFlail, weight = 12, },
            { template = namedItems.WeaponBloodHarp, weight = 12, },
            { template = namedItems.WeaponBloodLongsword, weight = 7, },
            { template = namedItems.WeaponBloodRapier, weight = 21, },
            { template = namedItems.WeaponBloodSpear, weight = 17, },
            { template = namedItems.WeaponBloodStaff, weight = 12, },
            { template = namedItems.WeaponBloodWarhammer, weight = 3, },
            { template = namedItems.WeaponBloodWhip, weight = 55, },

            -- glass weapons
            { template = namedItems.WeaponGlassAxe, weight = 33, },
            { template = namedItems.WeaponGlassBow, weight = 2, },
            { template = namedItems.WeaponGlassBroadsword, weight = 30, },
            { template = namedItems.WeaponGlassCat, weight = 27, },
            { template = namedItems.WeaponGlassCrossbow, weight = 2, },
            { template = namedItems.WeaponGlassCutlass, weight = 2, },
            { template = namedItems.WeaponGlassDagger, weight = 5, },
            { template = namedItems.WeaponGlassFlail, weight = 22, },
            { template = namedItems.WeaponGlassHarp, weight = 22, },
            { template = namedItems.WeaponGlassLongsword, weight = 41, },
            { template = namedItems.WeaponGlassRapier, weight = 33, },
            { template = namedItems.WeaponGlassSpear, weight = 32, },
            { template = namedItems.WeaponGlassStaff, weight = 22, },
            { template = namedItems.WeaponGlassWarhammer, weight = 2, },
            { template = namedItems.WeaponGlassWhip, weight = 36, },

            -- golden weapons
            { template = namedItems.WeaponGoldenAxe, weight = 16, },
            { template = namedItems.WeaponGoldenBow, weight = 2, },
            { template = namedItems.WeaponGoldenBroadsword, weight = 23, },
            { template = namedItems.WeaponGoldenCat, weight = 17, },
            { template = namedItems.WeaponGoldenCrossbow, weight = 1, },
            { template = namedItems.WeaponGoldenCutlass, weight = 2, },
            { template = namedItems.WeaponGoldenDagger, weight = 15, },
            { template = namedItems.WeaponGoldenFlail, weight = 17, },
            { template = namedItems.WeaponGoldenHarp, weight = 17, },
            { template = namedItems.WeaponGoldenLongsword, weight = 17, },
            { template = namedItems.WeaponGoldenRapier, weight = 16, },
            { template = namedItems.WeaponGoldenSpear, weight = 17, },
            { template = namedItems.WeaponGoldenStaff, weight = 17, },
            { template = namedItems.WeaponGoldenWarhammer, weight = 2, },
            { template = namedItems.WeaponGoldenWhip, weight = 18, },

            -- obsidian weapons
            { template = namedItems.WeaponObsidianAxe, weight = 32, },
            { template = namedItems.WeaponObsidianBow, weight = 3, },
            { template = namedItems.WeaponObsidianBroadsword, weight = 40, },
            { template = namedItems.WeaponObsidianCat, weight = 27, },
            { template = namedItems.WeaponObsidianCrossbow, weight = 3, },
            { template = namedItems.WeaponObsidianCutlass, weight = 4, },
            { template = namedItems.WeaponObsidianDagger, weight = 30, },
            { template = namedItems.WeaponObsidianFlail, weight = 15, },
            { template = namedItems.WeaponObsidianHarp, weight = 15, },
            { template = namedItems.WeaponObsidianLongsword, weight = 32, },
            { template = namedItems.WeaponObsidianRapier, weight = 32, },
            { template = namedItems.WeaponObsidianSpear, weight = 32, },
            { template = namedItems.WeaponObsidianStaff, weight = 15, },
            { template = namedItems.WeaponObsidianWarhammer, weight = 3, },
            { template = namedItems.WeaponObsidianWhip, weight = 22, },

            -- titanium weapons
            { template = namedItems.WeaponTitaniumAxe, weight = 39, },
            { template = namedItems.WeaponTitaniumBow, weight = 2, },
            { template = namedItems.WeaponTitaniumBroadsword, weight = 46, },
            { template = namedItems.WeaponTitaniumCat, weight = 22, },
            { template = namedItems.WeaponTitaniumCrossbow, weight = 2, },
            { template = namedItems.WeaponTitaniumCutlass, weight = 2, },
            { template = namedItems.WeaponTitaniumDagger, weight = 50, },
            { template = namedItems.WeaponTitaniumFlail, weight = 17, },
            { template = namedItems.WeaponTitaniumHarp, weight = 17, },
            { template = namedItems.WeaponTitaniumLongsword, weight = 34, },
            { template = namedItems.WeaponTitaniumRapier, weight = 34, },
            { template = namedItems.WeaponTitaniumSpear, weight = 39, },
            { template = namedItems.WeaponTitaniumStaff, weight = 36, },
            { template = namedItems.WeaponTitaniumWarhammer, weight = 2, },
            { template = namedItems.WeaponTitaniumWhip, weight = 37, },

            -- special daggers
            { template = namedItems.WeaponDaggerFrost, weight = 21, },
            { template = namedItems.WeaponDaggerPhasing, weight = 21, },
            { template = namedItems.WeaponDaggerElectric, weight = 40, },
            { template = namedItems.WeaponDaggerJeweled, weight = 8, },
            
            -- Guns
            { template = namedItems.WeaponBlunderbuss, weight = 3, },
            { template = namedItems.WeaponRifle, weight = 3, },

            
            -- feet
            { template = namedItems.FeetBootsExplorers, weight = 22, },
            { template = namedItems.FeetGlassSlippers, weight = 22, },
            { template = namedItems.FeetGreaves, weight = 11, },
            { template = namedItems.FeetBootsLead, weight = 22, },
            { template = namedItems.FeetBootsStrength, weight = 21, },
        },
    },
    TokenChestBlue = {
        name = "TokenChestBlue",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestPurple = {
        name = "TokenChestPurple",
        holder = "ChestPurple",
        spriteName = "token_chest.png",
        replacements = {
            -- rings
            { template = namedItems.RingBecoming, weight = 4, },
            { template = namedItems.RingCharisma, weight = 24, },
            { template = namedItems.RingCourage, weight = 15, },
            { template = namedItems.RingGold, weight = 24, },
            { template = namedItems.RingLuck, weight = 24, },
            { template = namedItems.RingMana, weight = 3, },
            { template = namedItems.RingPeace, weight = 12, },
            { template = namedItems.RingProtection, weight = 8, },
            { template = namedItems.RingRegeneration, weight = 20, },
            { template = namedItems.RingShielding, weight = 8, },
            { template = namedItems.RingWar, weight = 20, },
            { template = namedItems.RingFrost, weight = 4, },
            { template = namedItems.RingPiercing, weight = 20, },

            -- scrolls
            { template = namedItems.ScrollEarthquake, weight = 4, },
            { template = namedItems.ScrollEnchantWeapon, weight = 4, },
            { template = namedItems.ScrollFear, weight = 4, },
            { template = namedItems.ScrollFireball, weight = 8, },
            { template = namedItems.ScrollFreezeEnemies, weight = 8, },
            { template = namedItems.ScrollNeed, weight = 4, },
            { template = namedItems.ScrollRiches, weight = 6, },
            { template = namedItems.ScrollShield, weight = 8, },
            { template = namedItems.ScrollTransmute, weight = 5, },

            -- spells
            { template = namedItems.SpellBomb, weight = 32, },
            { template = namedItems.SpellFireball, weight = 40, },
            { template = namedItems.SpellFreezeEnemies, weight = 20, },
            { template = namedItems.SpellHeal, weight = 32, },
            { template = namedItems.SpellShield, weight = 32, },
            { template = namedItems.SpellTransmute, weight = 12, },
            { template = namedItems.SpellEarth, weight = 8, },
            { template = namedItems.SpellPulse, weight = 40, },

            -- tomes
            { template = namedItems.TomeEarth, weight = 8, },
            { template = namedItems.TomeFireball, weight = 8, },
            { template = namedItems.TomeFreeze, weight = 8, },
            { template = namedItems.TomeShield, weight = 8, },
            { template = namedItems.TomeTransmute, weight = 8, },
        },
    },
    TokenChestRed = {
        name = "TokenChestRed",
        holder = "ChestRed",
        spriteName = "token_chest.png",
        replacements = {
            -- action
            { template = namedItems.BloodDrum, weight = 50, },
            { template = namedItems.WarDrum, weight = 50, },
            { template = namedItems.ThrowingStars, weight = 110, },

            -- bomb
            { template = namedItems.Bomb, weight = 50, },
            { template = namedItems.Bomb3, weight = 25, },

            -- charm
            { template = namedItems.CharmStrength, weight = 5, },
            { template = namedItems.CharmGluttony, weight = 15, },
            { template = namedItems.CharmRisk, weight = 15, },
            { template = namedItems.CharmBomb, weight = 50, },
            { template = namedItems.CharmFrost, weight = 50, },
            { template = namedItems.CharmGrenade, weight = 50, },
            { template = namedItems.MiscMonkeyPaw, weight = 50, },
            { template = namedItems.CharmNazar, weight = 50, },
            { template = namedItems.CharmProtection, weight = 50, },

            -- familiar
            { template = namedItems.FamiliarDove, weight = 25, },
            { template = namedItems.FamiliarIceSpirit, weight = 25, },
            { template = namedItems.FamiliarRat, weight = 25, },
            { template = namedItems.FamiliarShopkeeper, weight = 25, },

            -- head
            { template = namedItems.HeadMinersCap, weight = 5, },
            { template = namedItems.HeadGlassJaw, weight = 25, },
            { template = namedItems.HeadHelm, weight = 50, },
            { template = namedItems.HeadBlastHelm, weight = 75, },
            { template = namedItems.HeadSpikedEars, weight = 75, },
            { template = namedItems.HeadSunglasses, weight = 75, },
            
            -- healing
            { template = namedItems.Food1, weight = 137, }, -- Apple
            { template = namedItems.Food2, weight = 87, }, -- Cheese
            { template = namedItems.Food3, weight = 87, }, -- Drumstick
            { template = namedItems.Food4, weight = 125, }, -- Ham
            { template = namedItems.FoodCarrot, weight = 87, },
            { template = namedItems.FoodCookies, weight = 87, },
            { template = namedItems.CursedPotion, weight = 125, },
            { template = namedItems.HolyWater, weight = 110, },

            -- misc
            { template = namedItems.HudBackpack, weight = 25, },
            { template = namedItems.MiscCoupon, weight = 50, },
            { template = namedItems.Holster, weight = 50, },

            -- shovels
            { template = namedItems.ShovelBattle, weight = 100, },
            { template = namedItems.ShovelCourage, weight = 70, },
            { template = namedItems.Pickaxe, weight = 70, },

            -- torches
            { template = namedItems.Torch1, weight = 165, },
            { template = namedItems.Torch2, weight = 161, },
            { template = namedItems.Torch3, weight = 241, },
            { template = namedItems.TorchForesight, weight = 125, },
            { template = namedItems.TorchGlass, weight = 247, },
            { template = namedItems.TorchInfernal, weight = 232, },
            { template = namedItems.TorchObsidian, weight = 125, },
            { template = namedItems.TorchStrength, weight = 5, },
            { template = namedItems.TorchWalls, weight = 125, },
        },
    },
    TokenChestTrap1 = {
        name = "TokenChestTrap1",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestTrap2 = {
        name = "TokenChestTrap2",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestTrap3 = {
        name = "TokenChestTrap3",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestTrap4 = {
        name = "TokenChestTrap4",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestTrap5 = {
        name = "TokenChestTrap5",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestTrap6 = {
        name = "TokenChestTrap6",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestCrate1 = {
        name = "TokenChestCrate1",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestCrate2 = {
        name = "TokenChestCrate2",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestCrate3 = {
        name = "TokenChestCrate3",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
    TokenChestCrate4 = {
        name = "TokenChestCrate4",
        spriteName = "token_chest.png",
        replacements = {
        },
    },
}

for _, token in utils.sortedPairs(tokens.items) do
    entities.addItem(token.template, {
        name = token.name,
        data = {
            slot = token.slot,
        },
        components = {
            item = {},
            sprite = {
                texture = "mods/SynchronyExtendedAPIPools/gfx/" .. token.spriteName,
                width = 18,
                height = 18,
            },
            SynchronyExtendedAPI_itemSubstitution = {},
        },
    })
end

return tokens